# search.py
# ---------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

"""
In search.py, you will implement generic search algorithms which are called 
by Pacman agents (in searchAgents.py).
"""

import util

class SearchProblem:
	"""
	This class outlines the structure of a search problem, but doesn't implement
	any of the methods (in object-oriented terminology: an abstract class).
	
	You do not need to change anything in this class, ever.
	"""
	
	def getStartState(self):
		 """
		 Returns the start state for the search problem 
		 """
		 util.raiseNotDefined()
		
	def isGoalState(self, state):
		 """
			 state: Search state
		
		 Returns True if and only if the state is a valid goal state
		 """
		 util.raiseNotDefined()

	def getSuccessors(self, state):
		 """
			 state: Search state
		 
		 For a given state, this should return a list of triples, 
		 (successor, action, stepCost), where 'successor' is a 
		 successor to the current state, 'action' is the action
		 required to get there, and 'stepCost' is the incremental 
		 cost of expanding to that successor
		 """
		 util.raiseNotDefined()

	def getCostOfActions(self, actions):
		 """
			actions: A list of actions to take
 
		 This method returns the total cost of a particular sequence of actions.  The sequence must
		 be composed of legal moves
		 """
		 util.raiseNotDefined()
					 

def tinyMazeSearch(problem):
	"""
	Returns a sequence of moves that solves tinyMaze.  For any other
	maze, the sequence of moves will be incorrect, so only use this for tinyMaze
	"""
	from game import Directions
	s = Directions.SOUTH
	w = Directions.WEST
	return  [s,s,w,s,w,w,s,w]

def depthFirstSearch(problem):
	"""
	Search the deepest nodes in the search tree first [p 85].
	
	Your search algorithm needs to return a list of actions that reaches
	the goal.  Make sure to implement a graph search algorithm [Fig. 3.7].
	
	To get started, you might want to try some of these simple commands to
	understand the search problem that is being passed in:
	
	print "Start:", problem.getStartState()
	print "Is the start a goal?", problem.isGoalState(problem.getStartState())
	print "Start's successors:", problem.getSuccessors(problem.getStartState())
	"""
	"*** YOUR CODE HERE ***"

	# Imports #
	from util import Stack
	from game import Directions

	# Variables #
	# Setting the frontier
	frontier = Stack()
	coords_seen_array = []
	# Code #
	frontier.push( (problem.getStartState(), [] , problem.getSuccessors(problem.getStartState()) ) )

	while not frontier.isEmpty():
		e = frontier.pop()

		# If the current element is the goal state, then return the next element
		if( problem.isGoalState(e[0]) ):
			return e[1]

		coords_seen_array.append( e[0] )

		for index in problem.getSuccessors(e[0]):

			if( not index[0] in coords_seen_array ):

				frontier.push( (index[0], e[1] + [index[1]] , problem.getSuccessors(index[0]) ) )


	util.raiseNotDefined()

def breadthFirstSearch(problem):
	"Search the shallowest nodes in the search tree first. [p 81]"
	"*** YOUR CODE HERE ***"

	# Imports #
	from util import Queue

	# Variables #
	frontier = Queue()
	coords_seen_array = []
	overflowcheck = False

	# Adding current state to frontier
	frontier.push( (problem.getStartState(), []) )

	while ( not frontier.isEmpty() and overflowcheck == False ):

		current_element, actions = frontier.pop()

		# For each sucessor from the current element
		for coordinates, direction, steps in problem.getSuccessors( current_element ):

			# If the coordinate has not been seen / visited
			if( not coordinates in coords_seen_array ):

				# If the coordinate is the goal state, return the actions and new direction
				if problem.isGoalState(coordinates):
					return actions + [direction]

				# Add the coordinates with the past actions and new direction to the frontier
				frontier.push( (coordinates, actions + [direction]) )
				# Add the coordinate to the seen array
				coords_seen_array.append( coordinates )

		# if the length of the coordinates seen array exceeds 10000, stop running the while loop and exit
		if( len(coords_seen_array) > 3000 ):
			overflowcheck = True
			print "coords_seen_array exceeded 3000 elements. Attempt Failed"

	util.raiseNotDefined()
			
def uniformCostSearch(problem):
	"Search the node of least total cost first. "
	"*** YOUR CODE HERE ***"

	# Imports
	from game import Directions

	# Variables #
	coords_seen_array = []

	# Adding current state to frontier
	frontier = [ ( problem.getStartState(), [] , problem.getSuccessors(problem.getStartState()) ) ]

	while ( len(frontier) > 0 ):

		e = min(frontier, key=lambda sortKey: sortKey[2][0][2])

		frontier.remove(e)

		if(problem.isGoalState(e[0])):
			return e[1]

		coords_seen_array.append( e[0] )

		for index in problem.getSuccessors( e[0] ):
			if( not index[0] in coords_seen_array ):
				frontier.append( (index[0], e[1] + [index[1]] , problem.getSuccessors(index[0]) ) )

	util.raiseNotDefined()

def nullHeuristic(state, problem=None):
	"""
	A heuristic function estimates the cost from the current state to the nearest
	goal in the provided SearchProblem.  This heuristic is trivial.
	"""
	return 0

def aStarSearch(problem, heuristic=nullHeuristic):
	"Search the node that has the lowest combined cost and heuristic first."
	"*** YOUR CODE HERE ***"

	# Variables
	visited_nodes = []
	frontier = util.PriorityQueue()
	startState = problem.getStartState()

	frontier.push( (startState, []), heuristic(startState, problem))

	while not frontier.isEmpty():

		# Get current node and actions from frontier
		current_node, actions = frontier.pop()

		# For each successors from the current node:
		for coordinates, direction, cost in problem.getSuccessors(current_node):

			# If node has not been visited before
			if not coordinates in visited_nodes:

				new_actions = actions + [direction]

				# If the current node is the goal state, return the set of actions to get to node state and exit function
				if problem.isGoalState(current_node):
					return new_actions

				# calculate total_cost
				total_cost = problem.getCostOfActions(new_actions) + heuristic(coordinates, problem)
				# Add the current coordinate and potential actions with total cost to frontier
				frontier.push( (coordinates, new_actions), total_cost)

				# If node is not the goal state, add node to visited_nodes
				visited_nodes.append(current_node)

	return [] 



	util.raiseNotDefined()
		
	
# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch