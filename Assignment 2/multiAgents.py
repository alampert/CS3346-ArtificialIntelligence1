# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
	"""
		A reflex agent chooses an action at each choice point by examining
		its alternatives via a state evaluation function.

		The code below is provided as a guide.  You are welcome to change
		it in any way you see fit, so long as you don't touch our method
		headers.
	"""


	def getAction(self, gameState):
		"""
		You do not need to change this method, but you're welcome to.

		getAction chooses among the best options according to the evaluation function.

		Just like in the previous project, getAction takes a GameState and returns
		some Directions.X for some X in the set {North, South, West, East, Stop}
		"""
		# Collect legal moves and successor states
		legalMoves = gameState.getLegalActions()

		# Choose one of the best actions
		scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
		bestScore = max(scores)
		bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
		chosenIndex = random.choice(bestIndices) # Pick randomly among the best

		"Add more of your code here if you want to"

		return legalMoves[chosenIndex]

	def evaluationFunction(self, currentGameState, action):
		"""
		Design a better evaluation function here.

		The evaluation function takes in the current and proposed successor
		GameStates (pacman.py) and returns a number, where higher numbers are better.

		The code below extracts some useful information from the state, like the
		remaining food (oldFood) and Pacman position after moving (newPos).
		newScaredTimes holds the number of moves that each ghost will remain
		scared because of Pacman having eaten a power pellet.

		Print out these variables to see what you're getting, then combine them
		to create a masterful evaluation function.
		"""
		# Useful information you can extract from a GameState (pacman.py)
		successorGameState = currentGameState.generatePacmanSuccessor(action)
		newPos = successorGameState.getPacmanPosition()
		oldFood = currentGameState.getFood()
		newGhostStates = successorGameState.getGhostStates()
		newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

		"*** YOUR CODE HERE ***"

		currentFood = successorGameState.getFood()

		# If the agent has won, return maximum score possible
		if successorGameState.isWin():
			return float("inf") - 20


		# If the agent hasn't won: Calculate the score based on the ghosts position.
		pos_ghost 				= currentGameState.getGhostPosition(1)			# Get ghost position
		dist_ghost 				= util.manhattanDistance(pos_ghost, newPos)		# Calculate Manhattan distance between current pos and ghost
		score 					= dist_ghost + successorGameState.getScore()	# Score = Ghost Distance + Successor score
		dist_pellet_shortest 	= 1000000										# Init the shortest distance to food to max


		# Find the shortest distance for each food pellet
		for pellet in currentFood.asList():
			# Get the manhattan distance between the new position and the pellet
			dist_pellet = util.manhattanDistance(pellet, newPos)	# Calc Distance to pellet
			if ( dist_pellet < dist_pellet_shortest ):				# If the pellet is closer than food, set the new food distance as the closest food distance
				dist_pellet_shortest = dist_pellet
		
		# add points to score of sucessor state if successor state is Pacman eating food.
		if ( currentGameState.getNumFood() > successorGameState.getNumFood() ):
			score += 100

		# Make sure Pacman is always moving
		if action == Directions.STOP:
			score -= 1000

		# Remove 3 points * the closest food val
		score -= 3 * dist_pellet_shortest
		
		list_capsules_avail = currentGameState.getCapsules()

		# If pacman's new position would be in one of the power up capsule places, add 120 points to that score
		if successorGameState.getPacmanPosition() in list_capsules_avail:
			score += 120

		if dist_ghost <= 2 & newScaredTimes[0] == 0:	# Avoid Ghost
			score -= 100000
		elif dist_ghost <= 10 & newScaredTimes[0] != 0:	# Fight Back Pacman! Revenge is sweet.
			score += 150

		return score

		# return successorGameState.getScore()

def scoreEvaluationFunction(currentGameState):
	"""
		This default evaluation function just returns the score of the state.
		The score is the same one displayed in the Pacman GUI.

		This evaluation function is meant for use with adversarial search agents
		(not reflex agents).
	"""
	return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
	"""
		This class provides some common elements to all of your
		multi-agent searchers.  Any methods defined here will be available
		to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

		You *do not* need to make any changes here, but you can if you want to
		add functionality to all your adversarial search agents.  Please do not
		remove anything, however.

		Note: this is an abstract class: one that should not be instantiated.  It's
		only partially specified, and designed to be extended.  Agent (game.py)
		is another abstract class.
	"""

	def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
		self.index = 0 # Pacman is always agent index 0
		self.evaluationFunction = util.lookup(evalFn, globals())
		self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
	"""
		Your minimax agent (question 2)
	"""

	def getAction(self, gameState):
		"""
			Returns the minimax action from the current gameState using self.depth
			and self.evaluationFunction.

			Here are some method calls that might be useful when implementing minimax.

			gameState.getLegalActions(agentIndex):
				Returns a list of legal actions for an agent
				agentIndex=0 means Pacman, ghosts are >= 1

			Directions.STOP:
				The stop direction, which is always legal

			gameState.generateSuccessor(agentIndex, action):
				Returns the successor game state after an agent takes an action

			gameState.getNumAgents():
				Returns the total number of agents in the game
		"""
		"*** YOUR CODE HERE ***"

		# Calculates min score from state
		def minScore(state, depth, agent_i):

			count_ghosts = gameState.getNumAgents() - 1;
			score = float("inf")
			legal_actions = state.getLegalActions(agent_i)

			if state.isWin() or state.isLose() or depth == 0:
				return self.evaluationFunction(state)

			if agent_i == count_ghosts:
				for action in legal_actions:
					score = min(score, maxScore(state.generateSuccessor(agent_i, action), depth - 1))

			else:
				for action in legal_actions:
					score = min(score, minScore(state.generateSuccessor(agent_i, action), depth, agent_i + 1))

			return score

		def maxScore(state, depth):

			count_ghosts = gameState.getNumAgents() - 1;
			score = -(float("inf"))

			if depth == 0 or state.isWin() or state.isLose():
				return self.evaluationFunction(state)

			for legal_actions in state.getLegalActions(0):

				# Score is max betwen last score and minScore function of next step
				score = max(score, minScore(state.generateSuccessor(0, legal_actions), depth - 1, 1))

			return score

		score = -(float("inf"))
		action = Directions.STOP

		for legal_actions in gameState.getLegalActions():

			if legal_actions != Directions.STOP:
				score_old = score
				state_next = gameState.generateSuccessor(0, legal_actions)

				score = max(score, minScore(state_next, self.depth, 1))

				if score > score_old:
					action = legal_actions

		return action

class AlphaBetaAgent(MultiAgentSearchAgent):
	"""
		Your minimax agent with alpha-beta pruning (question 3)
	"""

	def getAction(self, gameState):
		"""
			Returns the minimax action using self.depth and self.evaluationFunction
		"""
		"*** YOUR CODE HERE ***"
		def maxScore(state, alpha, beta, depth):

			if state.isWin() or state.isLose() or depth == 0:
				return self.evaluationFunction(state)

			score = -(float("inf"))
			legal_actions = state.getLegalActions(0)

			for action in legal_actions:
				nextState = state.generateSuccessor(0, action)
				score = max(score, minScore(nextState, alpha, beta, state.getNumAgents() - 1, depth))

				if score >= beta:
					return score

				alpha = max(alpha, score)

			return score
			
		
		def minScore(gameState, alpha, beta, agent_i, depth):

			numghosts = gameState.getNumAgents() - 1

			if gameState.isWin() or gameState.isLose() or depth == 0:
				return self.evaluationFunction(gameState)

			score = float("inf")
			legal_actions = gameState.getLegalActions(agent_i)

			for action in legal_actions:
				nextState = gameState.generateSuccessor(agent_i, action)

				if agent_i == numghosts:

					score = min(score, maxScore(nextState, alpha, beta, depth - 1))
					if score <= alpha:
						return score
					beta = min(beta, score)

				else:
					
					score = min(score, minScore(nextState, alpha, beta, agent_i + 1, depth))
					if score <= alpha:
						return score
					beta = min(beta, score)
					
			return score

		
		action = Directions.STOP

		score 	= -(float("inf"))
		alpha 	= -(float("inf"))
		beta 	= float("inf")

		for avail_action in gameState.getLegalActions(0):

			prevscore = score
			nextState = gameState.generateSuccessor(0, avail_action)
			score = max(score, minScore(nextState, alpha, beta, 1, self.depth))

			if score > prevscore:
				action = avail_action

			if score >= beta:
				return action
			
			alpha = max(alpha, score)

		return action

class ExpectimaxAgent(MultiAgentSearchAgent):
	"""
		Your expectimax agent (question 4)
	"""

	def getAction(self, gameState):
		"""
			Returns the expectimax action using self.depth and self.evaluationFunction

			All ghosts should be modeled as choosing uniformly at random from their
			legal moves.
		"""
		"*** YOUR CODE HERE ***"
		util.raiseNotDefined()

def betterEvaluationFunction(currentGameState):
	"""
		Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
		evaluation function (question 5).

		DESCRIPTION: <write something here so we know what you did>
	"""
	"*** YOUR CODE HERE ***"
	util.raiseNotDefined()

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
	"""
		Your agent for the mini-contest
	"""

	def getAction(self, gameState):
		"""
			Returns an action.  You can use any method you want and search to any depth you want.
			Just remember that the mini-contest is timed, so you have to trade off speed and computation.

			Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
			just make a beeline straight towards Pacman (or away from him if they're scared!)
		"""
		"*** YOUR CODE HERE ***"
		util.raiseNotDefined()

